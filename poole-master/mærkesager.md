---
layout: page
title: Mærkesager
---

<p class="message">
  På denne side kan du læse om partiets mærkesager.
</p>

## Anti-digitalisering
Menneskelig kommunikation er nu blevet til en glødende LCD skærm, som dikterer dit liv. Hvert mennesker går nu rundt med sådan en og det kræver destruerende mængder af energi, ressourcer og endda liv. Du kunne gå ud, møde dine venner ansigt til ansigt, lege og løbe rundt som da du var ung, men du er en slave til digitalismen selvom at det er unødigt. Skru ned for digitalismen, global opvarmning, spild af ressourcer, energi og så meget andet, som man ikke har brug for. Du ødelægger verden ellers.
## Transparens
Virkelighedspartiet tager ansvaret der ligger i at oplyse de individuelle borgere om den ufortolkede sandhed, på trods af hvor ubehagelig og dyster sandheden kan være. Derfor har virkelighedspartiet til opgave at oplyse befolkningen om vores fælles ansvar for at have en realistisk behandling af kloden og samfundet. For at opnå det mål skal befolkningen og partiet arbejde sammen for at opnå vores mål. Det gøres ved at alle partiets handlinger bliver udført i transparens så den aktive borger kan bidrage til den bedre og realistiske fremtid.

Derfor behøver partiet ikke en spindoktor, og der vil ikke blive set fænomener som greenwashing fra virkelighedspartiet. Der bliver derfor heller ikke lagt skjul på at  virkelighedspartiets politik kan føre til folke-hysteri, når partiet oplyser om sandheden uden at pakke den ind i bobbelplast. Partiet nægter at blive dobbeltmoralsk i håbet om at blive mere populær blandt befolkningen.