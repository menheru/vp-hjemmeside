---
layout: post
title: Hvad er virkelighedspartiet
---

Virkelighedspartiet er et dansk parti der netop nu er ved at indsamle vælgererklæringer så de kan stille op til folketingsvalget. Partiet har et ønske om at gøre en forskel for samfundet og klimaet.
